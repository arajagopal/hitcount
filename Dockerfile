FROM openjdk:8-jre-alpine
COPY ./build/libs/hitcount-0.0.1-SNAPSHOT.jar /usr/src/count/
WORKDIR /usr/src/count
EXPOSE 8081
CMD ["java", "-jar", "hitcount-0.0.1-SNAPSHOT.jar"]