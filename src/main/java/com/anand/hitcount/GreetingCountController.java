package com.anand.hitcount;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class GreetingCountController {
    private static final String template = "Hello from Spring, %s!";
    private static ConcurrentHashMap<String, AtomicInteger> countMap = new ConcurrentHashMap<>();

    @RequestMapping(value = "/increment/{name}", method = RequestMethod.PUT)
    public void count(@PathVariable(value = "name") String name) {
        if (countMap.containsKey(name)) {
            countMap.get(name).incrementAndGet();
        } else {
            countMap.put(name, new AtomicInteger(1));
        }
    }

    @RequestMapping(value = "/count/{name}", method = RequestMethod.GET)
    public Integer getCount(@PathVariable(value = "name") String name) {
        return countMap.getOrDefault(name, new AtomicInteger(0)).get();
    }
}